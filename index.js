const API_BASE_URL = "https://noroff-komputer-store-api.herokuapp.com";

//fetching HTML elements
const buttonGetALoanElement = document.getElementById("get-loan");
const buttonBankElement = document.getElementById("add-bank");
const buttonWorkElement = document.getElementById("btn-work"); 
const boughtLaptopElement = document.getElementById("buy-laptop"); 
const repaidLoanElement = document.getElementById("repay-loan");
const payAmountElement = document.getElementById("pay-amount");
const loanAmountElement = document.getElementById("loan-amount");
const bankAmountElement = document.getElementById("bank-amount");
const laptopFeaturesElement = document.getElementById("laptopfeatures");
const laptopsElement = document.getElementById("laptops");
const priceInformationElement = document.getElementById("price");
const titleInformationElement = document.getElementById("title");
const descriptionInformationElement = document.getElementById("description");
const imageInformationElement = document.getElementById("image-info");

let laptops = [];
let currentBalance = 0;
let currentLoan = 0;
let workSalary = 0;

//Hide repay loan button
document.getElementById("repay-loan").style.display = "none";
document.getElementById("hide-outstandingloan").style.display = "none";


// Fetch from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then(response => response.json())
.then(data => laptops = data)
.then(laptops => addLaptopsToList(laptops))

//Recieve laptops, loop on each laptop and run addLaptopsToList
const addLaptopsToList = (laptops) => {
    laptops.forEach(laptop => addLaptopToList(laptop));
    laptopFeaturesElement.innerText = laptops[0].specs;
    titleInformationElement.innerText = laptops[0].title;
    priceInformationElement.innerText = laptops[0].price + " NOK";
    descriptionInformationElement.innerText = laptops[0].description;
    imageInformationElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops [0].image;  
}

//Create DOM element and add title
const addLaptopToList = (laptop) => {
   const laptopsElement = document.getElementById("laptops");
   const laptopElement = document.createElement("option")
   laptopElement.innerText = laptop.title
   laptopsElement.appendChild(laptopElement)
}

//Fetch features and image
const featureLaptopsChange = e => {
   const selectedLaptops = laptops[e.target.selectedIndex];
   laptopFeaturesElement.innerText = selectedLaptops.specs;
   imageInformationElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops [selectedLaptops.id - 1].image;
   
}

//Fetch title, price and description from API
const laptopsInfo = e => {
   const infoLaptops = laptops[e.target.selectedIndex];
   titleInformationElement.innerText = infoLaptops.title;
   priceInformationElement.innerText = infoLaptops.price+ " NOK";
   descriptionInformationElement.innerText = infoLaptops.description;
}

//Add 100 NOK 'work' button
 const addPay = () => {
   workSalary += 100;
   payAmountElement.innerText = workSalary + " NOK";
}

//Get loan function
const getLoan = () => {
   if (currentLoan > 0) {
   return alert("Joe Banker will not allow you to have more than one loan")
   }
   else {
   const promptLoan = prompt("Enter amount to loan (up to double your bank balance)")
   if (promptLoan > currentBalance * 2)  {
           return alert("Joe Banker will not let you loan more than double your balance")
       }                    
   if (promptLoan === null) {
         return; 
     }
      else {
            const bankAmount = currentBalance + parseInt(promptLoan);
            currentLoan = promptLoan;
            currentBalance = bankAmount;
            bankAmountElement.innerText = bankAmount + " NOK";
            loanAmountElement.innerText = promptLoan + " NOK";
            document.getElementById("repay-loan").style.display = "block";
            document.getElementById("hide-outstandingloan").style.display = "block";
      }
   }
}

//Send pay to bank balance, with 10% sent to repay loan if one has an outstanding loan
const payBalanceLoan = () => {
   if (currentLoan > 0) {
       let ninetyPercent = (workSalary * 0.9);
       let tenPercent = (workSalary * 0.1);
       currentBalance += ninetyPercent;       
       currentLoan -= tenPercent;

      if (currentLoan < 0) {
         currentLoan = currentLoan *- 1;
         loanAmountElement.innerText = currentLoan + " NOK" ;
         bankAmountElement.innerText = currentBalance + " NOK" ;
         workSalary = 0;
         payAmountElement.innerText = 0; 
      } else {
         loanAmountElement.innerText = currentLoan + " NOK" ;
         bankAmountElement.innerText = currentBalance + "  NOK" ;
         workSalary = 0;
         payAmountElement.innerText = 0; 
      }
   }  
   else(currentLoan === 0)
       currentBalance += workSalary;
       bankAmountElement.innerText = currentBalance + " NOK";
       workSalary *= 0;
       payAmountElement.innerText = workSalary + " NOK";
}

//Buy laptop function
const buyLaptop = () => {
   const purchasePrice = parseInt(priceInformationElement.innerText)
      if (currentBalance >= purchasePrice) {
         currentBalance -= purchasePrice;
         bankAmountElement.innerText = currentBalance + " NOK"
         alert("Congratulations, you puchased a new laptop!")
      } else {
         alert("Not enough to purchase this laptop");
      }
}

//Repay loan function
const repayLoan = () => {
   if (currentLoan > 0) {
      currentLoan -= workSalary;
       loanAmountElement.innerText = currentLoan + " NOK";
       workSalary = 0;

       if (currentLoan < 0) {
       workSalary = currentLoan *- 1;
       payAmountElement.innerText = workSalary  + " NOK";
       loanAmountElement.innerText = 0 + " NOK";
       document.getElementById("repay-loan").style.display = "none";
       document.getElementById("hide-outstandingloan").style.display = "none";
      }  
   }
   
  if (currentLoan === 0) {
     document.getElementById("repay-loan").style.display = "none";
     document.getElementById("hide-outstandingloan").style.display = "none";
     workSalary = 0;
     payAmountElement.innerText = 0 + " NOK"; 
 }
}

laptopsElement.addEventListener('change', featureLaptopsChange);
buttonWorkElement.addEventListener('click', addPay);
laptopsElement.addEventListener('change', laptopsInfo);
buttonGetALoanElement.addEventListener('click', getLoan);
buttonBankElement.addEventListener('click', payBalanceLoan);
repaidLoanElement.addEventListener('click', repayLoan);
boughtLaptopElement.addEventListener('click', buyLaptop);