# assignment - Komputer Store app

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Assignment for Noroff Accelerate course - FrontEnd development with JavaScript

Not finished:
- Slight issues with Bootstrap CSS (under Joe banker headline)

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
```

## Usage

```
```

## Maintainers

[@stian.noroff](https://gitlab.com/stian.noroff)

## Contributing



Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Stian Ingebrigtsen
